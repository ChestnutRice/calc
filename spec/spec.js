const calc = require("../index");

describe("Calculator", function () {
    it("should add numbers", function () {
        expect(calc.add(2, 3)).toBe(5);
    });

    it("should subtract numbers", function () {
        expect(calc.subtract(5, 4)).toBe(1);
    });

    it("should multiply numbers", function () {
        expect(calc.multiply(4, 5)).toBe(20);
    });

    it("should divide numbers", function () {
        expect(calc.divide(6, 2)).toBe(3);
    });

    it("should power numbers", function () {
        expect(calc.power(6, 2)).toBe(36);
    });
});