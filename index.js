const add = function (a, b) {
    return a + b;
};

const subtract = function (a, b) {
    return a - b;
};

const multiply = function (a, b) {
    return a * b;
};

const divide = function (a, b) {
    if (b == 0) {
        throw new Error ("Second value can't be zero!");
    }
    return a / b;
};

const power = function (a, b) {
    return Math.pow(a, b);
};

module.exports.add = add;
module.exports.subtract = subtract;
module.exports.power = power;
module.exports.multiply = multiply;
module.exports.divide = divide;
