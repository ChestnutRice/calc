const gulp = require("gulp");
const jasmine = require('gulp-jasmine');

const calc = require("./index.js");

var args = require('minimist')(process.argv.slice(3));

gulp.task("add", function () {
    console.log(calc.add(args.a, args.b));
});

gulp.task("subtract", function () {
    console.log(calc.subtract(args.a, args.b));
});

gulp.task("multiply", function () {
    console.log(calc.multiply(args.a, args.b));
});

gulp.task("divide", function () {
    console.log(calc.divide(args.a, args.b));
});

gulp.task("power", function () {
    console.log(calc.power(args.a, args.b));
});

gulp.task("test", function () {
    gulp.src('./spec/spec.js').pipe(jasmine());
});